import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
Given two arrays, write a function to compute their intersection.

Example:
Given nums1 = [1, 2, 2, 1], nums2 = [2, 2], return [2, 2].

Note:
Each element in the result should appear as many times as it shows in both arrays.
The result can be in any order.
*/
public class LC350_Intersect {
	
	 public int[] intersect(int[] nums1, int[] nums2) {
         if (nums1 == null || nums2 == null || nums1.length == 0 || nums2.length == 0) {
           return new int[]{};
       }
       Map<Integer, Integer> map1 = getMap(nums1);
       Map<Integer, Integer> map2 = getMap(nums2);
       List<Integer> list = new ArrayList<>();
       for (Map.Entry<Integer, Integer> entry : map1.entrySet()) {
           Integer key = entry.getKey();
           Integer value = entry.getValue();
           if (map2.containsKey(key)) {
               int count = value < map2.get(key) ? value : map2.get(key);
               while (count != 0) {
                   list.add(key);
                   count--;
               }
           }
       }
       int i = 0;
       int[] res = new int[list.size()];
       for (int num : list) {
           res[i++] = num;
       }
       return res;
   }
   private Map<Integer, Integer> getMap(int[] nums) {
       Map<Integer, Integer> map = new HashMap<>();
       for (int num : nums) {
           map.put(num, map.getOrDefault(num, 0) + 1);
       }
       return map;
   }

	public static void main(String[] args) {
		int[] nums1 = {1, 2, 2, 1}, nums2 = {2, 2};
		LC350_Intersect test = new LC350_Intersect();
		int[] res = test.intersect(nums1, nums2);
		System.out.println(Arrays.toString(res));
	}

}
