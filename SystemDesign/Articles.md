2017  
4.3  
Numbers everyone should know.
<https://everythingisdata.wordpress.com/2009/10/17/numbers-everyone-should-know/>  
This article tells about the memory and disk access cost range. Fundamental of System Design.   

4.4  
Designs, Lessons and Advice from Building Large Distributed Systems  
<http://www.cs.cornell.edu/projects/ladis2009/talks/dean-keynote-ladis2009.pdf>  
