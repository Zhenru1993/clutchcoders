Welcome to ClutchCoders!  
Every day each team member at least finish one problem.  
If anyone fails to submit one problem for two consective days, he will have to leave the group.  
Happy coding, everyone, fight for your dream job!  
Please fork and create a pull request to commit to the repo, thank you!  

10 Team Members: Accomplishments  
Daisy: 11  
Qi Wang: 8  
EthanL: 8   
Snow Sun: 5  
Yu Zhang: 5  
Liz Tan: 5  
Xing: 3  
Wei He: 3  
Jenny Zheng: 2  
Jessica: 2  

3.31 Qi 349 Daisy 349 Yu 349 Liz 349  
4.1 Qi 283 350 Yu 350 Snow 349 350 Liz 350 Daisy 94 144 145 173 350 Jenny 350 Xing 350 EthanL 349 350  
4.2 Qi 21 88 Snow 88 Yu 88 Liz 88 Jess 88 Wei 88 350  
4.3 Qi 92 206 Snow 206 Jenny 206 Liz 206 Yu 206 Xing 88 206 EthanL 88 206 150 Daisy 206  
4.4 Qi 141 Snow 141 Yu 141 EthanL 20 32 84 Jess 141 Daisy 141 Liz 141 Wei 141