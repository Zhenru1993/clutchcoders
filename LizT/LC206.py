'''
Reverse a singly linked list.

click to show more hints.

Hint:
A linked list can be reversed either iteratively or recursively. Could you implement both?

Subscribe to see which companies asked this question.
'''

# Definition for singly-linked list.
# class ListNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class Solution(object):
    def reverseList(self, head):
        """
        :type head: ListNode
        :rtype: ListNode
        """
        if head is None:
            return head
        next_val = []
        while head.next:
            next_val.append(head.val)
            head = head.next
        rev = head
        while next_val:
            print(head.val)
            head.next = ListNode(next_val.pop())
            head = head.next
        return rev